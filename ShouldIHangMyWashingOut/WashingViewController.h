//
//  ViewController.h
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 25/05/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "WashingModel.h"

@interface WashingViewController : UIViewController <UIAlertViewDelegate>
                

@property (nonatomic, weak) IBOutlet UILabel *washingAnswer;
@property (nonatomic, weak) IBOutlet UILabel *washingQuip;
@property (nonatomic, weak) IBOutlet UIImageView *washingYesImage;
@property (nonatomic, weak) IBOutlet UIImageView *washingNoImage;
@property (nonatomic, weak) IBOutlet UILabel *userLocation;

@property (nonatomic, weak) IBOutlet UIButton *changeLocation;
@property (nonatomic, weak) IBOutlet UIButton *shareAnswer;

@property (nonatomic, strong) WashingModel *DataModel;

- (IBAction)changeLocationWasPressed:(id)sender;
- (IBAction)shareAnswerWasPressed:(id)sender;

-(int)getRandomNumberBetween:(int)from to:(int)to;

@end
