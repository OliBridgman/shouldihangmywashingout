//
//  BusyViewController.h
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 20/06/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void) updateTitle:(NSString *)newTitle;

+ (BusyViewController *)addToView:(UIView *)parentView withTitle: (NSString *)title;

@end
