//
//  ViewController.m
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 25/05/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import "WashingViewController.h"
#import "WashingModel.h"
#import "BusyViewController.h"
#import "QuartzCore/QuartzCore.h"

@interface WashingViewController ()
{
    BusyViewController *busyViewController;

}

@end

@implementation WashingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        busyViewController = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Setting up data model/Notification stuff
    self.DataModel = [WashingModel sharedManager];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataResponse:)
                                                 name:@"weatherResult"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataResponse:)
                                                 name:@"addressResult"
                                               object:nil];
    busyViewController = [BusyViewController addToView:self.view withTitle:@"Loading..."];
    [self.DataModel applicationBeginning];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) refreshUI
{
    
    //Allow to check two hours in advance. Maybe not needed.

    NSString *datamodelAnswer = self.DataModel.answer;
    self.userLocation.text = self.DataModel.address;
    NSString *quip = self.DataModel.quip;
    
    //All Dem Nos
    if ([datamodelAnswer isEqualToString:@"dark"] || [datamodelAnswer isEqualToString:@"rain"]
        || [datamodelAnswer isEqualToString:@"sleet"] || [datamodelAnswer isEqualToString:@"fog"]
        || [datamodelAnswer isEqualToString:@"willRain"] || [datamodelAnswer isEqualToString:@"snow"])
    {
        self.washingAnswer.text = @"NO";
        self.washingAnswer.textColor = [UIColor whiteColor];
        self.washingAnswer.backgroundColor = [UIColor colorWithRed:237.0f / 255.0f green:0.0f / 255.0f blue:13.0f / 255.0f alpha:1];
        self.washingAnswer.layer.cornerRadius = 5;
        self.washingQuip.text = quip;
        self.washingNoImage.hidden = NO;
        self.washingYesImage.hidden = YES;
    }
    //All of the YESS
    else if([datamodelAnswer isEqualToString:@"clear-day"] || [datamodelAnswer isEqualToString:@"partly-cloudy-day"] || [datamodelAnswer isEqualToString:@"cloudy"])
    {
        self.washingAnswer.text = @"YES";
        self.washingAnswer.textColor = [UIColor whiteColor];
        self.washingAnswer.backgroundColor = [UIColor colorWithRed:0.0f / 255.0f green:132.0f / 255.0f blue:237.0f / 255.0f alpha:1];
        self.washingAnswer.layer.cornerRadius = 5;
        self.washingQuip.text = quip;
        self.washingNoImage.hidden = YES;
        self.washingYesImage.hidden = NO;
    }
    //All of maybes
    else if([datamodelAnswer isEqualToString:@"wind"])
    {
        self.washingAnswer.text = @"MAYBE";
        self.washingAnswer.backgroundColor = [UIColor colorWithRed:0.0f / 255.0f green:132.0f / 255.0f blue:237.0f / 255.0f alpha:1];
        self.washingAnswer.layer.cornerRadius = 5;
        self.washingQuip.text = quip;
        self.washingNoImage.hidden = NO;
        self.washingYesImage.hidden = YES;
    }
    //Errors
    else
    {
        NSLog(@"%@", datamodelAnswer);
        self.washingAnswer.text = @"FUCK";
        self.washingAnswer.backgroundColor = [UIColor colorWithRed:0.0f / 255.0f green:132.0f / 255.0f blue:237.0f / 255.0f alpha:1];
        self.washingAnswer.layer.cornerRadius = 5;
        self.washingQuip.text = quip;
        self.washingNoImage.hidden = NO;
        self.washingYesImage.hidden = YES;
    }
}



- (IBAction)changeLocationWasPressed:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Change Location"
                                                        message:@"Enter a new location:"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Go", nil];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
}

- (IBAction)shareAnswerWasPressed:(id)sender
{
    NSLog(@"Share Button");
}

#pragma mark - Data response

- (void) dataResponse: (NSNotification *) notification
{
    NSLog(@"notification is %@", [notification.object class]);
    if ([notification.object isKindOfClass:[NSString class]])
    {
        if (busyViewController != nil)
        {
            [busyViewController.view removeFromSuperview];
            busyViewController = nil;
        }
        [self refreshUI];
        /*
        DataRequest *dataRequest = (DataRequest *)notification.object;
        
        if (dataRequest == nil)
        {
            // This is not the request we are looking for...
            NSLog(@"Wrong request");
            return;
        }
        
        if (self.weatherRequest != nil)
        {
            NSLog(@"Here");
        }
         */
    }
}

#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Go"])
    {
        NSString *newLocation = [alertView textFieldAtIndex:0].text;
        busyViewController = [BusyViewController addToView:self.view withTitle:@"Loading..."];
        [self.DataModel geocodeLocation:newLocation];
    }
}
@end
