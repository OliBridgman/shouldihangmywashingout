//
//  WashingModel.m
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 25/05/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import "WashingModel.h"
#import "AFNetworking.h"

@implementation WashingModel
{
    BOOL haveCheckedSecondTime;
}

@synthesize answer, locationManager, quipsDictionary, address, quip;

#pragma mark Singleton Methods

+ (id)sharedManager
{
    static WashingModel *sharedMyManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        answer = @"";
        address = @"";
        quip = @"";
        
        //Setting up location services
        locationManager = [[CLLocationManager alloc] init];
        geocoder = [[CLGeocoder alloc] init];
        knownTimeZoneNames = [NSTimeZone knownTimeZoneNames];
        appTimeZone = [NSTimeZone localTimeZone];
        appDateFormatter = [[NSDateFormatter alloc] init];
        [appDateFormatter setTimeZone:appTimeZone];
    }
    return self;
}

-(void)getIcon:(Boolean)twoHours :(NSString *)lat :(NSString *)lon
{
    
    NSString *const BaseURLString = @"https://api.forecast.io/forecast/ca7621286e896812fbadbd082faeab47/";
    
    NSString *weatherUrl;
    __block NSString *icon;
    
    //Block to allow for time finding, two hours ahead.
    NSDate *mydate = [NSDate date];
    NSTimeInterval secondsInTwoHours = 2 * 60 * 60;
    NSDate *dateTwoHoursAhead = [mydate dateByAddingTimeInterval:secondsInTwoHours];
    
    
    
    time_t unixTime = (time_t) [dateTwoHoursAhead timeIntervalSince1970];
    
    
    if(twoHours)
    {
        weatherUrl = [NSString stringWithFormat:@"%@%@,%@,%li", BaseURLString, lat, lon, unixTime];
        haveCheckedSecondTime = YES;
    }
    else
    {
        weatherUrl = [NSString stringWithFormat:@"%@%@,%@", BaseURLString, lat, lon];
    }
    NSURL *url = [NSURL URLWithString:weatherUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
     
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSMutableDictionary *json1 = [[NSMutableDictionary alloc] initWithDictionary:JSON];
                                                        NSLog(@"JSON %@", [json1 description]);
                                                        NSArray *keys = [json1 allKeys];
                                                        id aKey = [keys objectAtIndex:0];
                                                        id rightNowData = [json1 objectForKey:aKey];
                                                        
                                                        icon = [rightNowData objectForKey:@"icon"];
                                                        self.answer = icon;
                                                        if (!haveCheckedSecondTime)
                                                        {
                                                            if([icon isEqualToString:@"cloudy"] || [icon isEqualToString:@"partly-cloudy-day"])
                                                            {
                                                                self.answer = @"cloudy";
                                                                [self getIcon:YES :lat :lon];
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if ([icon isEqualToString:@"rain"])
                                                            {
                                                                self.answer = @"willRain";
                                                            }
                                                        }
                                                        if([icon isEqualToString:@"partly-cloudy-day"])
                                                        {
                                                            self.answer = @"cloudy";
                                                        }

                                                        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
                                                        
                                                        NSInteger currentHour = [components hour];
                                                        NSLog(@"current hour is %li", (long)currentHour);
                                                        if(currentHour < 7 || currentHour >= 19 ||
                                                           [icon rangeOfString:@"night"].location != NSNotFound)
                                                        {
                                                            self.answer = @"dark";
                                                            NSLog(@"set to dark");
                                                        }
                                                        quip = [self generateQuipFromPlist];
                                                        
                                                        NSNotification *newNotification = [NSNotification notificationWithName:@"weatherResult" object:icon];
                                                        [[NSNotificationCenter defaultCenter] postNotification:newNotification];
                                                    }
                                                    //Called on API Failure
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        
                                                        NSLog(@"Failed with this error: %@", error);
                                                        NSNotification *newNotification = [NSNotification notificationWithName:@"weatherResult" object:icon];
                                                        [[NSNotificationCenter defaultCenter] postNotification:newNotification];
                                                        self.answer = @"error";
                                                        
                                                    }];
    
    //Start API operation
    [operation start];
    
    //    NSLog(@" icon before return %@", icon);
    
    //return icon;
    do {
    } while ([operation isExecuting]);
    //    NSLog(@"Has finished");
    
    //NSLog(@"notification centre sent");
}


-(void)getCurrentLocation
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}

-(void)reverseGeocoding
{
    CLLocation *currentLocation = [locationManager location];
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            //NSLog(@"first is %@", placemark.addressDictionary);
            self.address = [NSString stringWithFormat:@"%@, %@", [placemark.addressDictionary objectForKey:@"City"], placemark.country];
            NSNotification *newNotification = [NSNotification notificationWithName:@"addressResult" object:address];
            [[NSNotificationCenter defaultCenter] postNotification:newNotification];
            
        } else {
            self.answer = @"error";
            NSNotification *newNotification = [NSNotification notificationWithName:@"addressResult" object:address];
            [[NSNotificationCenter defaultCenter] postNotification:newNotification];
            NSLog(@"%@", error.debugDescription);
        }
    } ];
}

-(void)geocodeLocation: (NSString *)newLocation
{
    NSLog(@"geocode called");
    [geocoder geocodeAddressString:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
    {
        if ([placemarks count] > 0)
        {
            NSLog(@"placemarks %@", [placemarks description]);
            CLPlacemark *markplace = [placemarks lastObject];
            NSString *lat = [NSString stringWithFormat:@"%f", markplace.location.coordinate.latitude];
            NSString *lon = [NSString stringWithFormat:@"%f", markplace.location.coordinate.longitude];
            //NSLog(@"markplace is %@", markplace.addressDictionary);
            //NSLog(@"lat %@, lon %@", lat, lon);
            NSString *city = [markplace.addressDictionary objectForKey:@"City"];
            NSString *country = markplace.country;
            if ([city isKindOfClass:[NSString class]])
            {
                self.address = [NSString stringWithFormat:@"%@, %@", city, country];
            }
            else
            {
                self.address = [NSString stringWithFormat:@"%@", country];
            }
            NSNotification *newNotification = [NSNotification notificationWithName:@"addressResult" object:address];
            [[NSNotificationCenter defaultCenter] postNotification:newNotification];
            //[self reverseGeocoding:parsedLocation];
            [self getIcon:NO :lat :lon];
        }
        else {
            self.answer = @"error";
            NSNotification *newNotification = [NSNotification notificationWithName:@"addressResult" object:address];
            [[NSNotificationCenter defaultCenter] postNotification:newNotification];
            NSLog(@"%@", error.debugDescription);
        }

    }];
}

#pragma mark - CLLocationManagerDelegate
/**
 ~
 ~
 ~
 **/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

/**
 ~
 ~
 ~
 **/
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [locationManager stopUpdatingLocation];
}


/**
 Things to do when the application has begun or when things need to be refreshed.
 **/
-(void) applicationBeginning
{
    [self getCurrentLocation];
    NSString *lat = [NSString stringWithFormat:(@"%f"), [[self locationManager] location].coordinate.latitude];
    NSString *lon = [NSString stringWithFormat:(@"%f"), [[self locationManager]
                                                         location].coordinate.longitude];
    
    NSLog(@"lat %@, lon %@", lat, lon);
    [self getIcon:NO :lat :lon];
    [self reverseGeocoding];
}

// This will eventually be pulling a random quip from our plist. Defined by the icon returned
// by Forecast.

- (NSString *)generateQuipFromPlist
{
    NSString *path = [[NSBundle mainBundle] pathForResource:
                      @"quips" ofType:@"plist"];
    
    // Build the array from the plist
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    NSArray *value = [dict valueForKey:self.answer];
    NSLog(@"answer is %@", self.answer);
    NSLog(@"Quip is %@", [value objectAtIndex:[self getRandomNumberBetween:0 to:[value count]]]);
    return [value objectAtIndex:[self getRandomNumberBetween:0 to:[value count]]];
}


-(int)getRandomNumberBetween:(int)from to:(int)to {
    
    return arc4random_uniform(to);
}


@end
