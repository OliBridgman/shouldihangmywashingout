//
//  AppDelegate.h
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 25/05/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
