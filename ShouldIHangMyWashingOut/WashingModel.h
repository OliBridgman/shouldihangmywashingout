//
//  WashingModel.h
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 25/05/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"
#import <CoreLocation/CoreLocation.h>


@interface WashingModel : NSObject
            <CLLocationManagerDelegate>
{
    NSString __block *answer;
    NSString __block *address;
    NSString __block *quip;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSArray *knownTimeZoneNames;
    NSTimeZone *appTimeZone;
    NSDateFormatter *appDateFormatter;
}

@property (nonatomic, copy) __block NSString *answer;
@property (nonatomic, copy) CLLocationManager *locationManager;
@property (nonatomic, copy) NSDictionary *quipsDictionary;
@property (nonatomic, copy)__block  NSString *address;
@property (nonatomic, copy)__block NSString *quip;


+(id) sharedManager;

-(void)applicationBeginning;
-(void)geocodeLocation: (NSString *)newLocation;
-(void)getIcon:(Boolean)twoHours :(NSString *)lat :(NSString *)lon;


@end
