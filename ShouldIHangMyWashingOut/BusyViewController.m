//
//  BusyViewController.m
//  ShouldIHangMyWashingOut
//
//  Created by Oliver on 20/06/13.
//  Copyright (c) 2013 dulltone. All rights reserved.
//

#import "BusyViewController.h"

@interface BusyViewController ()

@property NSString *titleText;

@end

@implementation BusyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateTitle];
}

- (void)viewDidUnload
{
    [self setTitleLabel:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateTitle
{
    if ([self.titleText length] == 0)
    {
        self.titleLabel.text = @"";
        self.titleLabel.hidden = YES;
    }
    else
    {
        self.titleLabel.text = self.titleText;
        self.titleLabel.hidden = NO;
    }
}

- (void) updateTitle:(NSString *)newTitle
{
    self.titleText = newTitle;
    [self updateTitle];
}

+ (BusyViewController *)addToView:(UIView *)parentView withTitle: (NSString *)title
{
    UIWindow *window = parentView.window;
    
    BusyViewController *busyViewController = [[BusyViewController alloc] initWithNibName:nil bundle:nil];
    busyViewController.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    
    busyViewController.titleText = title;
    
    busyViewController.view.frame = CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height);
    
    //    [parentView addSubview:busyViewController.view];
    [window addSubview:busyViewController.view];
    
    return busyViewController;
}

@end
