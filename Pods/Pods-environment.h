
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 0
#define COCOAPODS_VERSION_MINOR_AFNetworking 9
#define COCOAPODS_VERSION_PATCH_AFNetworking 1

// Evernote-SDK-iOS
#define COCOAPODS_POD_AVAILABLE_Evernote_SDK_iOS
#define COCOAPODS_VERSION_MAJOR_Evernote_SDK_iOS 1
#define COCOAPODS_VERSION_MINOR_Evernote_SDK_iOS 2
#define COCOAPODS_VERSION_PATCH_Evernote_SDK_iOS 0

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 5
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 3

// JSONKit
#define COCOAPODS_POD_AVAILABLE_JSONKit
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.5pre.

// SBJson
#define COCOAPODS_POD_AVAILABLE_SBJson
#define COCOAPODS_VERSION_MAJOR_SBJson 3
#define COCOAPODS_VERSION_MINOR_SBJson 2
#define COCOAPODS_VERSION_PATCH_SBJson 0

// SSKeychain
#define COCOAPODS_POD_AVAILABLE_SSKeychain
#define COCOAPODS_VERSION_MAJOR_SSKeychain 0
#define COCOAPODS_VERSION_MINOR_SSKeychain 2
#define COCOAPODS_VERSION_PATCH_SSKeychain 1

// ShareKit
#define COCOAPODS_POD_AVAILABLE_ShareKit
#define COCOAPODS_VERSION_MAJOR_ShareKit 2
#define COCOAPODS_VERSION_MINOR_ShareKit 3
#define COCOAPODS_VERSION_PATCH_ShareKit 0

// ShareKit/Core
#define COCOAPODS_POD_AVAILABLE_ShareKit_Core
#define COCOAPODS_VERSION_MAJOR_ShareKit_Core 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Core 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Core 0

// ShareKit/Evernote
#define COCOAPODS_POD_AVAILABLE_ShareKit_Evernote
#define COCOAPODS_VERSION_MAJOR_ShareKit_Evernote 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Evernote 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Evernote 0

// ShareKit/Facebook
#define COCOAPODS_POD_AVAILABLE_ShareKit_Facebook
#define COCOAPODS_VERSION_MAJOR_ShareKit_Facebook 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Facebook 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Facebook 0

// ShareKit/Flickr
#define COCOAPODS_POD_AVAILABLE_ShareKit_Flickr
#define COCOAPODS_VERSION_MAJOR_ShareKit_Flickr 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Flickr 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Flickr 0

// ShareKit/Foursquare
#define COCOAPODS_POD_AVAILABLE_ShareKit_Foursquare
#define COCOAPODS_VERSION_MAJOR_ShareKit_Foursquare 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Foursquare 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Foursquare 0

// ShareKit/GoogleReader
#define COCOAPODS_POD_AVAILABLE_ShareKit_GoogleReader
#define COCOAPODS_VERSION_MAJOR_ShareKit_GoogleReader 2
#define COCOAPODS_VERSION_MINOR_ShareKit_GoogleReader 3
#define COCOAPODS_VERSION_PATCH_ShareKit_GoogleReader 0

// ShareKit/Instapaper
#define COCOAPODS_POD_AVAILABLE_ShareKit_Instapaper
#define COCOAPODS_VERSION_MAJOR_ShareKit_Instapaper 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Instapaper 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Instapaper 0

// ShareKit/LinkedIn
#define COCOAPODS_POD_AVAILABLE_ShareKit_LinkedIn
#define COCOAPODS_VERSION_MAJOR_ShareKit_LinkedIn 2
#define COCOAPODS_VERSION_MINOR_ShareKit_LinkedIn 3
#define COCOAPODS_VERSION_PATCH_ShareKit_LinkedIn 0

// ShareKit/Pinboard
#define COCOAPODS_POD_AVAILABLE_ShareKit_Pinboard
#define COCOAPODS_VERSION_MAJOR_ShareKit_Pinboard 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Pinboard 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Pinboard 0

// ShareKit/ReadItLater
#define COCOAPODS_POD_AVAILABLE_ShareKit_ReadItLater
#define COCOAPODS_VERSION_MAJOR_ShareKit_ReadItLater 2
#define COCOAPODS_VERSION_MINOR_ShareKit_ReadItLater 3
#define COCOAPODS_VERSION_PATCH_ShareKit_ReadItLater 0

// ShareKit/Tumblr
#define COCOAPODS_POD_AVAILABLE_ShareKit_Tumblr
#define COCOAPODS_VERSION_MAJOR_ShareKit_Tumblr 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Tumblr 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Tumblr 0

// ShareKit/Twitter
#define COCOAPODS_POD_AVAILABLE_ShareKit_Twitter
#define COCOAPODS_VERSION_MAJOR_ShareKit_Twitter 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Twitter 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Twitter 0

// ShareKit/Vkontakte
#define COCOAPODS_POD_AVAILABLE_ShareKit_Vkontakte
#define COCOAPODS_VERSION_MAJOR_ShareKit_Vkontakte 2
#define COCOAPODS_VERSION_MINOR_ShareKit_Vkontakte 3
#define COCOAPODS_VERSION_PATCH_ShareKit_Vkontakte 0

// objectiveflickr
#define COCOAPODS_POD_AVAILABLE_objectiveflickr
#define COCOAPODS_VERSION_MAJOR_objectiveflickr 2
#define COCOAPODS_VERSION_MINOR_objectiveflickr 0
#define COCOAPODS_VERSION_PATCH_objectiveflickr 2

